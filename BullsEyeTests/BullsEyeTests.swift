/// Copyright (c) 2019 Razeware LLC
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy
/// of this software and associated documentation files (the "Software"), to deal
/// in the Software without restriction, including without limitation the rights
/// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
/// copies of the Software, and to permit persons to whom the Software is
/// furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in
/// all copies or substantial portions of the Software.
/// 
/// Notwithstanding the foregoing, you may not use, copy, modify, merge, publish,
/// distribute, sublicense, create a derivative work, and/or sell copies of the
/// Software in any work that is designed, intended, or marketed for pedagogical or
/// instructional purposes related to programming, coding, application development,
/// or information technology.  Permission for such use, copying, modification,
/// merger, publication, distribution, sublicensing, creation of derivative works,
/// or sale is expressly withheld.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
/// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
/// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
/// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
/// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
/// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
/// THE SOFTWARE.

import XCTest





@ testable import BullsEye
class BullsEyeTests: XCTestCase {
  
  // Tell teh test case about what project you want to test
  // Give this file access to the function and variables from that file
  
  var game : BullsEyeGame!
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
      super.setUp();
      game = BullsEyeGame();
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
      
      game = nil;
      super.tearDown();
    }

  func testStartNewGame(){
    // Start a new game
    game.startNewGame();
     // Check that satring round number = 1
    XCTAssertEqual(1, game.round)
    // Check that total starting point = 0
    XCTAssertEqual(0, game.scoreTotal)
  }
  
  
  // R2: Test that a new round is cretaed.
  // A1 : Current round counter increrases by 1
  // A2 : Point for current round = 0
  // A3 : A new random target number is generated
  func testStartNewRound(){
    // 1. Start a new game
    // ----------------------
    game.startNewGame()
    let initialRoundNumber = 1
    let initialRoundPoints = 0
    let initialTargetNumber = game.targetValue
    
    // tests to check the initial state of the game
    XCTAssertEqual(initialRoundNumber, game.round)
    XCTAssertEqual(initialRoundPoints, game.scoreRound)
    
    // 2. Start a new round
    // ----------------------
    game.startNewRound()
    
    // 3. Check the current round number
    // -------------------------
    XCTAssertEqual(initialRoundNumber+1, game.round)
    
    // 4. Check the points for the round
    // -------------------------
    XCTAssertEqual(0, game.scoreRound)
    
    // 5. Check that you have a random number as the target
    // -------------------------
    // check the new target number is different from initial target number
    XCTAssertNotEqual(initialTargetNumber, game.targetValue)
    
    
    
  }

}
